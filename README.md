# edex-ui

A cross-platform, customizable science fiction terminal emulator with advanced monitoring & touchscreen support.

https://github.com/GitSquared/edex-ui

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/edex-ui.git
```

